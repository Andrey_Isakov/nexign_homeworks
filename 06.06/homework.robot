# Hаверняка, здесь есть что улучшить. Напишите мне в телеграмме, пожалуйста: @andrey_isakov01
*** Settings ***
Library  Collections

*** Variables ***
@{NUMBERS}  ${1}  ${2}  ${3}  ${5}  ${1}  ${0}  ${-1}  ${10}

*** Test Cases ***
Get the max and min values of a list
    ${max}  Set Variable  ${NUMBERS}[${0}]
    ${min}  Set Variable  ${NUMBERS}[${0}]
    ${len}  Get Length  ${NUMBERS}
    FOR  ${index}  IN RANGE  1  ${len}
        ${max}  Set Variable If  ${max} < ${NUMBERS}[${index}]  ${NUMBERS}[${index}]  ${max}
        ${min}  Set Variable If  ${min} > ${NUMBERS}[${index}]  ${NUMBERS}[${index}]  ${min}
    END
    Log  ${max} ${min}
    Set Test Message  Max value: ${max}; Min value: ${min}              # Доп. текстовое описание для отчетa

Get unique list values
    ${unique_values}  Create List
    FOR  ${num}  IN  @{NUMBERS}
        Run Keyword If  ${num} in ${unique_values}  Continue For Loop
        Append To List  ${unique_values}  ${num}
    END
    Log  ${unique_values}
    Set Test Message  List unique values: ${unique_values}

Get the sum of the list values
    ${sum}  Set Variable  ${0}
    FOR  ${num}  IN  @{NUMBERS}
        ${sum} =  Evaluate  ${sum} + ${num}
    END
    Log  ${sum}
    Set Test Message  Sum of the list values: ${sum}

Celsius to Fahrenheit conversion
    Cels to Fahr  ${0}  0  ${350}  eqwewq  ${-32}  ${100}               # Передача тестовых данных из теста

*** Keywords ***
Cels to Fahr
    [Arguments]  @{values}              # Объединение всех проверок в один тест, логика похожая на *args в python
    FOR  ${tc}  IN  @{values}
        ${is int}=  Evaluate  isinstance($tc, int)
        IF  ${is_int}
            ${tf} =  Evaluate  9/5*${tc} + 32               # Вычисление в кейворде
            Log  ${tf}
            Set Test Message  *HTML* <b>${tc}</b> in Celsius equal to <b>${tf}</b> in Fahrenheit\n  append=yes

        ELSE
            Log    Incorrect input value
            Set Test Message  *HTML* Input: <b>${tc}</b>; Incorrect input value\n  append=yes
        END
    END