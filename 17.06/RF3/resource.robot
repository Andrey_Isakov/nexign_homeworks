*** Settings ***
Library     RequestsLibrary     WITH NAME   Req
Library     PostgreSQLDB        WITH NAME   DB
Library     JsonValidator       WITH NAME   Js
Library     Collections         WITH NAME   Col
Library     framework.FrameWork    WITH NAME   FW
Test Timeout   3s
*** Keywords ***
Test Setup
    Req.Create Session          alias     http://localhost:3000
    DB.Connect To Postgresql    app_db    app_user   password    localhost   5432

Test Teardown
    Req.Delete All Sessions
    DB.Disconnect From Postgresql