*** Settings ***
Resource           ./resource.robot

*** Variables ***
${CATEGORYNAME}      Fantasy
${SQL_QUERY}         SELECT categoryname
...                  FROM bootcamp.categories
...                  WHERE categoryname=%(categoryname)s
@{RESULT_PARAMS}     categoryname
&{DB_PARAMS}         categoryname=${CATEGORYNAME}
&{INSERT_DATA}       categoryname=${CATEGORYNAME}

*** Test Cases ***
Check Data Insertion
    [Documentation]    Checking the insertion of new data into a table
    [Tags]             Insertion
    [Timeout]          1s
    FW.Insert Data To Table    alias    categories    ${INSERT_DATA}
    ${rest_result}    FW.Get Data From Rest    alias=alias    table_name=categories
...                                            params=categoryname=eq.${CATEGORYNAME}
...                                            expected_status=200
    ${categoryname}    FW.Filter Data From Rest    ${rest_result}    ${RESULT_PARAMS}
    ${db_result}       FW.Get Data From DB         ${SQL_QUERY}      ${DB_PARAMS}
    FW.Compare Results    ${db_result}    ${categoryname}[0]
