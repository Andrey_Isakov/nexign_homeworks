from robot.libraries.BuiltIn import BuiltIn
from JsonValidator  import JsonValidator

class FrameWork:

    builtin_lib: BuiltIn = BuiltIn()
    js = JsonValidator()

    def get_lib(self, lib_name: str):
        return self.builtin_lib.get_library_instance(lib_name)

    def get_data_from_rest(self, alias, table_name, params, expected_status):
        result = self.get_lib("Req").get_on_session(alias=alias, url=f'/{table_name}?',
                                                    params=params, expected_status=expected_status)
        return result.json()

    def filter_data_from_rest(self, json, params):
        res = []
        for param in params:
            res.append(self.js.get_elements(json, f'$..{param}'))
        return res

    def get_data_from_db(self, sql_req, params):
        return self.get_lib("DB").execute_sql_string(sql_req, **params)

    def compare_results(self, db_res, *rest_res):
        rest_res = list(zip(*rest_res))
        return self.get_lib("Col").lists_should_be_equal(rest_res, db_res)

    def insert_data_to_table(self, alias, table_name, data, expected_status='201'):
        self.get_lib("Req").post_on_session(alias=alias, url=f'/{table_name}',
                                            json=data, expected_status=expected_status)
