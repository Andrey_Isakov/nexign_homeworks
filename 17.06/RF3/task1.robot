*** Settings ***
Resource         ./resource.robot

*** Variables ***
${LIMIT}            ${10}
${SQL QUERY}        SELECT firstname, lastname, title
...                 FROM bootcamp.cust_hist
...                 LEFT OUTER JOIN bootcamp.customers
...                 ON cust_hist.customerid = customers.customerid
...                 LEFT OUTER JOIN bootcamp.products
...                 ON cust_hist.prod_id = products.prod_id
...                 LIMIT %(limit)s
@{RESULT_PARAMS}    firstname    lastname    title
&{DB_PARAMS}        limit=${LIMIT}

*** Test Cases ***
Check Filtering
    [Documentation]    Checking if data from rest and db match
    [Tags]             Filtering
    ${rest_result}  FW.Get Data From Rest    alias=alias    table_name=cust_hist
...                                          params=select=customers(firstname,lastname),products(title)&limit=10
...                                          expected_status=200
    ${firstname}  ${lastname}  ${title}    FW.Filter Data From Rest    ${rest_result}  ${RESULT_PARAMS}
    ${db_result}    FW.Get Data From DB    ${SQL QUERY}    ${DB_PARAMS}
    FW.Compare Results    ${db_result}    ${firstname}    ${lastname}    ${title}
