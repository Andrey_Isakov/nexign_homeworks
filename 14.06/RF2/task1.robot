*** Settings ***
Resource         ./resource.robot

*** Variables ***
${LIMIT}      ${10}
${SQL QUERY}  SELECT firstname, lastname, title
...           FROM bootcamp.cust_hist
...           LEFT OUTER JOIN bootcamp.customers
...           ON cust_hist.customerid = customers.customerid
...           LEFT OUTER JOIN bootcamp.products
...           ON cust_hist.prod_id = products.prod_id
...           LIMIT %(limit)s

*** Test Cases ***
Check Filtering
    ${firstname}    ${lastname}    ${title}    Get Name And Product Title From PostREST
...    params=select=customers(firstname,lastname),products(title)&limit=10
    @{result}          Get Name And Product Title From DB   limit=${LIMIT}
    Compare Results    ${result}    ${firstname}    ${lastname}    ${title}

*** Keywords ***
Get Name And Product Title From PostREST
    [Arguments]     ${params}
    ${resp}         Req.GET On Session    alias    /cust_hist?
...    params=${params}
    ${firstname}    Js.get elements    ${resp.json()}    $..firstname
    ${lastname}     Js.get elements    ${resp.json()}    $..lastname
    ${title}        Js.get elements    ${resp.json()}    $..title
    [Return]        ${firstname}    ${lastname}    ${title}

Get Name And Product Title From DB
    [Arguments]    &{args}
    ${params}      create dictionary               &{args}
    @{result}      DB.Execute Sql String Mapped    ${SQL_QUERY}    &{params}
    [Return]       @{result}

Compare results
    [Arguments]  ${result_from_db}  ${firstname}  ${lastnames}  ${title}
    ${firstnames_db}    Create List
    ${lastnames_db}     Create List
    ${title_db}         Create List
    FOR  ${k}  IN  @{result_from_db}
        Col.Append To List    ${firstnames_db}    ${k}[firstname]
        Col.Append To List    ${lastnames_db}     ${k}[lastname]
        Col.Append To List    ${title_db}         ${k}[title]
    END
    Col.Lists Should Be Equal    ${firstname}    ${firstnames_db}
    Col.Lists Should Be Equal    ${lastnames}    ${lastnames_db}
    Col.Lists Should Be Equal    ${title}        ${title_db}