*** Settings ***
Resource        ./resource.robot

*** Variables ***
${CATEGORYNAME}    Fantasy
${SQL_QUERY}       SELECT categoryname
...                FROM bootcamp.categories
...                WHERE categoryname=%(categoryname)s

*** Test Cases ***
Check Data Insertion
    Insert Data To Categories    ${CATEGORYNAME}
    ${result_rest}    Get Inserted Data From Categories With PostREST    params=categoryname=eq.${CATEGORYNAME}
    ${result_db}      Get Inserted Data From Categories With DB          categoryname=${CATEGORYNAME}
    Should Be Equal   ${result_rest}[0]    ${result_db}[categoryname]

*** Keywords ***
Insert Data To Categories
    [Arguments]    ${categoryname}
    &{data}        Create dictionary      categoryname=${categoryname}
    ${resp}        Req.POST On Session    alias           /categories
...                                       json=${data}    expected_status=anything
    Status Should Be    201    ${resp}
Get Inserted Data From Categories With PostREST
    [Arguments]    ${params}
    ${resp}        Req.GET On Session    alias    /categories?
...    params=${params}
    ${result}      Js.get elements    ${resp.json()}    $..categoryname
    [Return]       ${result}
Get Inserted Data From Categories With DB
    [Arguments]    &{args}
    ${params}      Create Dictionary               &{args}
    @{result}      DB.Execute Sql String Mapped    ${SQL_QUERY}    &{params}
    [Return]       ${result}[0]